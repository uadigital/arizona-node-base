# Node.js base image with the Jekyll static web site builder

The base is a stable version of the official Node.js, with the Jekyll static
web site builder added into this from a full Ruby development environment
(through a multi-stage build defibed in the Dockerfile), but leaving the Ruby
development tools behind. The official AWS command-line tools defined as a
Python package are also in the image, but the installation for these uses
binary (Wheel) packages, so does not require a full Python development
environment. The jq package that is also included is a JSON parser, which helps
with OAuth authentication when Bitbucket Pipelines use this image.

Note that the Gemfile and Gemfile.lock files determine the Jekyll build, and
are also in the final built image.
